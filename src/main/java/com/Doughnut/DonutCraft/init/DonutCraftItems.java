package com.Doughnut.DonutCraft.init;

import com.Doughnut.DonutCraft.Reference;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class DonutCraftItems {

    //Items
    public static Item donutGem;


    //Donut Tools
    public static Item ItemDonutSword;
    public static Item ItemDonutPick;
    public static Item ItemDonutShovel;
    public static Item ItemDonutAxe;
    public static Item ItemDonutHoe;

    //Emerald Tools
    public static Item ItemEmeraldSword;
    public static Item ItemEmeraldPick;
    public static Item ItemEmeraldShovel;
    public static Item ItemEmeraldAxe;
    public static Item ItemEmeraldHoe;


    //Materials
    public static Item.ToolMaterial DonutMaterial = EnumHelper.addToolMaterial("DonutMaterial", 5, 3500, 15F, 5F, 25);
    public static Item.ToolMaterial EmeraldMaterial = EnumHelper.addToolMaterial("EmeraldMaterial", 4, 2561, 9.0F, 4.0F, 15);


    public static void init() {
        donutGem = new Item().setUnlocalizedName("donutGem");

        //Donut Tools
        ItemDonutSword = new ItemDonutSword(DonutMaterial).setUnlocalizedName("donutSword");
        ItemDonutPick = new ItemDonutPick(DonutMaterial).setUnlocalizedName("donutPick");
        ItemDonutShovel = new ItemDonutShovel(DonutMaterial).setUnlocalizedName("donutShovel");
        ItemDonutAxe = new ItemDonutAxe(DonutMaterial).setUnlocalizedName("donutAxe");
        ItemDonutHoe = new ItemDonutHoe(DonutMaterial).setUnlocalizedName("donutHoe");

        //Emerald Tools
        ItemEmeraldSword = new ItemEmeraldSword(EmeraldMaterial).setUnlocalizedName("emeraldSword");
        ItemEmeraldPick = new ItemEmeraldPick(EmeraldMaterial).setUnlocalizedName("emeraldPick");
        ItemEmeraldShovel = new ItemEmeraldShovel(EmeraldMaterial).setUnlocalizedName("emeraldShovel");
        ItemEmeraldAxe = new ItemEmeraldAxe(EmeraldMaterial).setUnlocalizedName("emeraldAxe");
        ItemEmeraldHoe = new ItemEmeraldHoe(EmeraldMaterial).setUnlocalizedName("emeraldHoe");



    }

    public static void register()
    {

        GameRegistry.registerItem(donutGem, donutGem.getUnlocalizedName().substring(5));


        //Donut Tools
        GameRegistry.registerItem(ItemDonutSword, ItemDonutSword.getUnlocalizedName().substring(5));
        GameRegistry.registerItem(ItemDonutPick, ItemDonutPick.getUnlocalizedName().substring(5));
        GameRegistry.registerItem(ItemDonutShovel, ItemDonutShovel.getUnlocalizedName().substring(5));
        GameRegistry.registerItem(ItemDonutAxe, ItemDonutAxe.getUnlocalizedName().substring(5));
        GameRegistry.registerItem(ItemDonutHoe, ItemDonutHoe.getUnlocalizedName().substring(5));

        //Emerald Tools
        GameRegistry.registerItem(ItemEmeraldAxe, ItemEmeraldAxe.getUnlocalizedName().substring(5));
        GameRegistry.registerItem(ItemEmeraldHoe, ItemEmeraldHoe.getUnlocalizedName().substring(5));
        GameRegistry.registerItem(ItemEmeraldPick, ItemEmeraldPick.getUnlocalizedName().substring(5));
        GameRegistry.registerItem(ItemEmeraldShovel, ItemEmeraldShovel.getUnlocalizedName().substring(5));
        GameRegistry.registerItem(ItemEmeraldSword, ItemEmeraldSword.getUnlocalizedName().substring(5));


    }


    public static void registerRenders()
    {
        RegisterRender(donutGem);

        //Donut Tools
        RegisterRender(ItemDonutSword);
        RegisterRender(ItemDonutPick);
        RegisterRender(ItemDonutShovel);
        RegisterRender(ItemDonutAxe);
        RegisterRender(ItemDonutHoe);

        //Emerald Tools
        RegisterRender(ItemEmeraldAxe);
        RegisterRender(ItemEmeraldHoe);
        RegisterRender(ItemEmeraldPick);
        RegisterRender(ItemEmeraldShovel);
        RegisterRender(ItemEmeraldSword);

    }

    public static void RegisterRender(Item item)
    {
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
    }
}
