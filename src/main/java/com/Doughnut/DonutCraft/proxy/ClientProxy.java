package com.Doughnut.DonutCraft.proxy;

import com.Doughnut.DonutCraft.init.DonutCraftItems;

public class ClientProxy extends CommonProxy{
    @Override
    public void registerRenders(){
        DonutCraftItems.registerRenders();
    }
}
