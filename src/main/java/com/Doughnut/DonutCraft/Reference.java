package com.Doughnut.DonutCraft;

public class Reference {
    public static final String MOD_ID = "dc";
    public static final String MOD_NAME = "DonutCraft";
    public static final String VERSION =  "1.8-0.0.1";
    public static final String CLIENT_PROXY_CLASS = "com.Doughnut.DonutCraft.proxy.ClientProxy";
    public static final String SERVER_PROXY_CLASS = "com.Doughnut.DonutCraft.proxy.CommonProxy";

}
